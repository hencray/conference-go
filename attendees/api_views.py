from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from .models import Attendee
from events.models import Conference
from common.json import ModelEncoder
from events.api_views import ConferenceDetailEncoder
from django.views.decorators.http import require_http_methods
import json

class AttendeeEncoder(ModelEncoder):
    model = Attendee
    properties = ["name", "email", "company_name", "created"]
    encoders = {
        "conference": ConferenceDetailEncoder(),
    }

    def get_extra_data(self, obj):
        # Add any additional data, like 'href' if needed
        return {"href": obj.get_api_url()} if hasattr(obj, 'get_api_url') else {}

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference_id=conference_id)
        return JsonResponse({"attendees": attendees}, encoder=AttendeeEncoder)
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # a = Attendee.objects.get(id=id)

    # attendee_details = {
    #     "email": a.email,
    #     "name": a.name,
    #     "company_name": a.company_name,
    #     "created": a.created,
    #     "conference": {
    #         "name": a.conference.name,
    #         "href": a.conference.get_api_url(),  # Make sure Conference model has get_api_url method
    #     }
    # }
    # return JsonResponse(attendee_details)
    attendee = get_object_or_404(Attendee, id=id)
    return JsonResponse(attendee, encoder=AttendeeEncoder, safe=False)
